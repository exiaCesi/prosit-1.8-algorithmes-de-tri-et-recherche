#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

FILE* fichier1 = NULL;  //
FILE* fichier2 = NULL;  //
FILE* fichier3 = NULL;  //
                        //
int k, m, n;            //definition de ces variables en global


typedef struct {      //structure pour les données
  int dist;
  char nom[30];
  char prenom[30];
  int com;
} donnees;

void shellsort(donnees arr[], int num, int a)   //fonction pour le tri, a est un paramètre pour choisir si on tri par collectivité ou par distance
{
  long i, j, k, tmp1, tmp4;
  char tmp2[30], tmp3[30];
  switch(a)
  {
    case 1:                                     //tri par collectivité
    for (i = num / 2; i > 0; i = i / 2)
    {
      for (j = i; j < num; j++)
      {
        for(k = j - i; k >= 0; k = k - i)
        {
          if (arr[k+i].com >= arr[k].com)
            break;
          else
          {
            tmp1 = arr[k].dist;
            arr[k].dist = arr[k+i].dist;
            arr[k+i].dist = tmp1;

            tmp4 = arr[k].com;
            arr[k].com = arr[k+i].com;
            arr[k+i].com = tmp4;

            strcpy(tmp2, arr[k].nom);
            strcpy(arr[k].nom, arr[k+i].nom);
            strcpy(arr[k+i].nom, tmp2);

            strcpy(tmp3, arr[k].prenom);
            strcpy(arr[k].prenom, arr[k+i].prenom);
            strcpy(arr[k+i].prenom, tmp3);
          }
        }
      }
    }
    break;
    case 2:                                     //tri par distance
    for (i = num / 2; i > 0; i = i / 2)
    {
      for (j = i; j < num; j++)
      {
        for(k = j - i; k >= 0; k = k - i)
        {
          if (arr[k+i].dist >= arr[k].dist)
            break;
          else
          {
            tmp1 = arr[k].dist;
            arr[k].dist = arr[k+i].dist;
            arr[k+i].dist = tmp1;

            tmp4 = arr[k].com;
            arr[k].com = arr[k+i].com;
            arr[k+i].com = tmp4;

            strcpy(tmp2, arr[k].nom);
            strcpy(arr[k].nom, arr[k+i].nom);
            strcpy(arr[k+i].nom, tmp2);

            strcpy(tmp3, arr[k].prenom);
            strcpy(arr[k].prenom, arr[k+i].prenom);
            strcpy(arr[k+i].prenom, tmp3);
          }
        }
      }
    }
    break;
  }
}



void read(FILE *f, int k, donnees tab[])        //lis le fichier apres avoir ete defini et ajoute dans un tableau mis en paramètre
{
  for (int i=0 ; i<k ; i++)
  {
    fscanf(f, "%i %s %s %i", &tab[i].dist, &tab[i].nom, &tab[i].prenom, &tab[i].com);
  }
  fclose(f);
  shellsort(tab, k, 2);                         //tri les valeurs lus
}


void write(FILE *f, int k, donnees tab[])     //ecris dans un fichier en fonction de k , du fichier, du tableau
{
  for (int i=0 ; i<k ; i++)
  {
    fprintf(f, "%i %s %s %i\n", tab[i].dist, tab[i].nom, tab[i].prenom, tab[i].com);
  }
  fclose(f);
}

void write2(FILE *f1, FILE *f, int k)       //deuxieme type d'ecriture car permet de lire les infos dans le fichier et d'ecrire directement apres
{                                           //dans le gros fichier final
  donnees tab[1];

  for (int i=0 ; i<k ; i++)
  {
    fscanf(f1, "%i %s %s %i", &tab[0].dist, &tab[0].nom, &tab[0].prenom, &tab[0].com);
    fprintf(f, "%i %s %s %i\n", tab[0].dist, tab[0].nom, tab[0].prenom, tab[0].com);
  }
  fclose(f1);
}

void repartition(FILE *f1, FILE *f2, FILE *f3, int l, donnees tab[])  //reparti dans 3 fichiers differents les personnes en fonction de la collectivité
{
  for (int i=0 ; i<l ; i++)
  {
    if (tab[i].com == 971)
    {
      fprintf(fichier1, "%i %s %s %i\n", tab[i].dist, tab[i].nom, tab[i].prenom, tab[i].com);
      k+=1;
    }
    else if (tab[i].com == 977)
    {
      fprintf(fichier2, "%i %s %s %i\n", tab[i].dist, tab[i].nom, tab[i].prenom, tab[i].com);
      m+=1;
    }
    else if (tab[i].com == 978)
    {
      fprintf(fichier3, "%i %s %s %i\n", tab[i].dist, tab[i].nom, tab[i].prenom, tab[i].com);
      n+=1;
    }
  }
  fclose(fichier1);
  fclose(fichier2);
  fclose(fichier3);
}






int main()
{
  int a, b, i, l;
  clock_t h1, h2;
  float temps;
  h1 = clock();
  l = 500000;                             //definition du nombre de personnes

  donnees* tab = NULL;
  tab = malloc(l * sizeof(donnees));      //allocation de la memoire
  if (tab==NULL)
  {
    exit(0);
  }


  printf("coucou\n");

  FILE* fichier = NULL;
  fichier = fopen("evacuation_plan0.txt", "r");

  if (fichier != NULL)          //lecture du premier fichier et ajout dans un tableau des donnees
  {
    for (i=0 ; i<l ; i++)
    {
      fscanf(fichier, "%i %s %s %i", &tab[i].dist, &tab[i].nom, &tab[i].prenom, &tab[i].com);
    }
    shellsort(tab, l, 1);       //premier tri en fonction des collectivité
  }
  else
  {
    printf("Impossible d'ouvrir le fichier.");
  }
  fclose(fichier);



  donnees* tab1 = NULL;
  tab1 = malloc(l * sizeof(donnees));   //allocation de la memoire
  if (tab1==NULL)
  {
    exit(0);
  }

  donnees* tab2 = NULL;
  tab2 = malloc(l * sizeof(donnees));   //allocation de la memoire
  if (tab2==NULL)
  {
    exit(0);
  }

  donnees* tab3 = NULL;
  tab3 = malloc(l * sizeof(donnees));   //allocation de la memoire
  if (tab3==NULL)
  {
    exit(0);
  }
  k=0;
  m=0;
  n=0;


  fichier1 = fopen("resultat1.txt", "w+");

  fichier2 = fopen("resultat2.txt", "w+");

  fichier3 = fopen("resultat3.txt", "w+");

  repartition(fichier1, fichier2, fichier3, l, tab);  //repartition en 3 fichiers


  fichier1 = fopen("resultat1.txt", "r");

  read(fichier1, k, tab1);  //lecture et tri

  fichier1 = fopen("resultat1.txt", "w+");

  write(fichier1, k, tab1);   //réecriture dans le meme fichier

  fichier2 = fopen("resultat2.txt", "r");

  read(fichier2, m, tab2);

  fichier2 = fopen("resultat2.txt", "w+");

  write(fichier2, m, tab2);

  fichier3 = fopen("resultat3.txt", "r");

  read(fichier3, n, tab3);

  fichier = fopen("resultat3.txt", "w+");

  write(fichier3, n, tab3);


  fichier = fopen("output.txt", "w+");
  fichier1 = fopen("resultat1.txt", "r");

  write2(fichier1, fichier, k);   //lecture du premier fichier secondaire et ecriture dans le gros fichier final

  fichier2 = fopen("resultat2.txt", "r");

  write2(fichier2, fichier, m);

  fichier3 = fopen("resultat3.txt", "r");

  write2(fichier3, fichier, n);

  fclose(fichier);

  free(tab);                    //liberation de la memoire occupée par le tableau
  free(tab1);
  free(tab2);
  free(tab3);

  remove("resultat1.txt");    //suppression des fichiers temporaires
  remove("resultat2.txt");
  remove("resultat3.txt");

  h2 = clock();

  temps = (float)(h2-h1)/CLOCKS_PER_SEC;
  printf("temps = %f\n", temps);    //affichage du temps d'execution

  return 0;
}
