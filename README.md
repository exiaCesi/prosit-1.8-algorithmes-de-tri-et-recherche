# Algorithmes de Tri

Ce dépôt GitLab contient des codes sources proposés par différents élèves du Cesi.

Le dépôt est découpé en plusieurs dossiers. Le dossier "Ressources Prosit" contient les solutions pour les deux codes proposés dans les ressources du prosit. Les autres dossiers sont nommés par centre et contiennent les solutions proposées par un centre donné.

## Crédits :
- Les solutions pour les ressources du prosit sont proposées par Kevin Andrieux du centre de Aix en Provence.